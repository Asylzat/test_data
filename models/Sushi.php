<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sushi".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $price
 */
class Sushi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sushi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'price'], 'required'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 300],
            [['price'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'price' => 'Price',
        ];
    }
}
