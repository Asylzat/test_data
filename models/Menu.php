<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $prize
 * @property string $weight
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'prize', 'weight'], 'required'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 300],
            [['prize'], 'string', 'max' => 100],
            [['weight'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'prize' => 'Prize',
            'weight' => 'Weight',
        ];
    }
}
