<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Sushi */

$this->title = 'Create Sushi';
$this->params['breadcrumbs'][] = ['label' => 'Sushis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sushi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
