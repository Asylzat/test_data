<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sushi */

$this->title = 'Update Sushi: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Sushis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sushi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
